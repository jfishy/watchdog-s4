import time, json
from flask import Flask, request, Response
from flask_restful import Resource, Api, reqparse
from controller import queryDB

app = Flask(__name__)
api = Api(app)

model = queryDB()

class HealthCheck(Resource):
    def get(self):
        return {'status{}': 'alive{}'}, 200


# todo implement a get friends stored procedure
class getFriends(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                            type=str,
                            required=True,
                            help='user id')
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        friends = model.getUserFriends(args.username)
        return {'friends' : friends}


class login(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                            type=str,
                            required=True)
        parser.add_argument('password',
                            type=str,
                            required=True)
        self.parser = parser

    def post(self):
        args = self.parser.parse_args()
        token = model.login(args.username, args.password)
        if(token):
            return {'token': token}
        else:
            return None


class getGroups(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groups = model.getUserGroups(args.username)  # todo : query database
        return {'groups' : groups}


class getUserInfo(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True,
                                 help='user id')
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        info = model.getUserInfo(args.username)
        return {'info' : info}


class addPendingGroupMembership(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True,
                                 help='user id')
        parser.add_argument('groupname',
                            type=str,
                            required=True,
                            help='group name')
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.addPendingGroupMembership(userid, groupid)
        return "Added"


class addUserGroupMembership(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True,
                                 help='user id')
        parser.add_argument('groupname',
                            type=str,
                            required=True,
                            help='group name')
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groupid = model.getgroupId(args.groupname)
        model.addUserGroupMembership(args.username, groupid)
        return "Added"


class deletePendingGroupMembership(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True,
                                 help='user id')
        parser.add_argument('groupname',
                            type=str,
                            required=True,
                            help='group name')
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groupid = model.getgroupId(args.groupname)
        userid = model.getUserId(args.username)
        model.deletePendingGroupMembership(userid, groupid)
        return 'Removed'

class home(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                            type=str,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groups = model.getUserGroups(args.username)
        info = model.getUserInfo(args.username)
        return {'groups' : groups, 'info' : info}

class createGroup(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        """parser.add_argument('owner_username',
                            type=str,
                            required=True)"""
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('populationlimit',
                            type=str,
                            required=True)
        parser.add_argument('public',
                            type=int,
                            required=True)
        parser.add_argument('icon',
                            type=str,
                            required=True)
        parser.add_argument('description',
                            type=str,
                            required=True)
        parser.add_argument('inviteonly',
                            type=str,
                            required=True)
        self.parser = parser

    def post(self):
        args = self.parser.parse_args()
        model.createGroup(args.populationlimit, args.groupname, args.icon, args.public, args.inviteonly, args.description)
        return 'Created'

class getGroupInfo(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('groupname',
                            type=int,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groupid = model.getgroupId(args.groupname)
        groupinfo = model.getGroupInfo(groupid)
        return groupinfo

class getGroupMembers(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('groupname',
                                 type=int,
                                 required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groupid = model.getgroupId(args.groupname)
        groupmembers = model.getGroupMembers(groupid) 
        return {'groupmembers' : groupmembers}

class getGroupOwner(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('groupname',
                                 type=int,
                                 required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        groupid = model.getgroupId(args.groupname)
        groupowner = model.getGroupOwner(groupid) 
        return {'groupowner' : groupowner}

class createUser(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                            type=str,
                            required=True)
        parser.add_argument('password',
                            type=str,
                            required=True)
        parser.add_argument('firstName',
                            type=str,
                            required=True)
        parser.add_argument('lastName',
                            type=str,
                            required=True)
        parser.add_argument('email',
                            type=str,
                            required=True)
        parser.add_argument('city',
                            type=str,
                            required=True)
        parser.add_argument('country',
                            type=str,
                            required=True)
        self.parser = parser

    def post(self):
        args = self.parser.parse_args()
        model.createUser(args.username, args.password, args.firstName, args.lastName, args.email, args.city, args.country)
        return 'Created'

class deleteUser(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('password',
                            type=str,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        model.deleteUser(args.username, args.password)
        return 'Removed'

class validateUser(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.deleteUser(userid, groupid)
        return 'Valid'

class changeCapacity(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('capacity',
                            type=int,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.deleteUser(groupid, userid, args.capacity)
        return 'Updated'

class deleteGroup(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.deleteGroup(groupid, userid)
        return 'Updated'

class changeGroupName(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('newname',
                            type=str,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.changeGroupId(groupid, userid, args.newname)
        return 'Updated'

class changeGroupPrivacy(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('privacy',
                            type=int,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.changePrivacy(groupid, userid, args.privacy)
        return 'Updated'

class changeInviteOnly(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('invite',
                            type=int,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.changeInviteOnly(groupid, userid, args.invite)
        return 'Updated'

class changeGroupDescription(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('description',
                            type=str,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        model.changeGroupDescription(groupid, userid, args.description)
        return 'Updated'

class changeGroupOwner(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username',
                                 type=str,
                                 required=True)
        parser.add_argument('groupname',
                            type=str,
                            required=True)
        parser.add_argument('username_other',
                            type=int,
                            required=True)
        self.parser = parser

    def get(self):
        args = self.parser.parse_args()
        userid = model.getUserId(args.username)
        groupid = model.getgroupId(args.groupname)
        newOwner = model.getUserId(args.username_other)
        model.changeGroupOwner(groupid, userid, newOwner)
        return 'Updated'

api.add_resource(HealthCheck, '/healthcheck')
api.add_resource(getFriends, '/getfriends')
api.add_resource(login, '/login')
api.add_resource(getGroups, '/getgroups')
api.add_resource(getUserInfo, '/getuserinfo')
api.add_resource(addPendingGroupMembership, '/addpendinggroupmembership')
api.add_resource(addUserGroupMembership, '/addusergroupmembership')
api.add_resource(deletePendingGroupMembership, '/deletependinggroupmembership')
api.add_resource(home, '/home')
api.add_resource(createGroup, '/creategroup')
api.add_resource(getGroupInfo, '/getgroupinfo')
api.add_resource(getGroupMembers, '/getgroupmembers')
api.add_resource(getGroupOwner, '/getgroupowner')
api.add_resource(createUser, '/createuser')
api.add_resource(deleteUser, '/deleteuser')
api.add_resource(validateUser, '/validateuser')
api.add_resource(changeCapacity, '/changecapacity')
api.add_resource(deleteGroup, '/deletegroup')
api.add_resource(changeGroupName, '/changename')
api.add_resource(changeGroupPrivacy, '/changeprivacy')
api.add_resource(changeInviteOnly, '/changeinviteonly')
api.add_resource(changeGroupDescription, '/changedesciption')
api.add_resource(changeGroupOwner, '/changeowner')

# todo get group owner id (owner_id) done
# todo get group members done
# todo change method for everything in group settings
# todo make sure verify user is user in group done
# todo create/delete user done
# todo siren sends message to all group members as notification, set off siren flag within group



if __name__ == "__main__":
    app.run(host='localhost', port='7050')