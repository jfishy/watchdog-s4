import mysql.connector
import json
import hashlib

class queryDB:

    def __init__(self):

        self.mydb = mysql.connector.connect(
            host='localhost',
            user='watchdog',
            password='watchdog',
            database='watchdog_server',
            auth_plugin='mysql_native_password'
        )

    def getUserId(self, userName):
        mycursor = self.mydb.cursor()
        mycursor.execute('Call getUserId(\'{}\')'.format(userName))
        try:
            return mycursor.fetchall()[0][0]
        except IndexError:
            return 'User Does Not Exist'

    def getgroupId(self, groupName):
        mycursor = self.mydb.cursor()
        mycursor.execute('select * from watchdog_groups where group_name = \'{}\''.format(groupName))
        return mycursor.fetchall()[0][0]

    def getUserInfo(self, userName):
        mycursor = self.mydb.cursor()
        mycursor.execute('Call getUserInfo(\'{}\')'.format(userName))
        return mycursor.fetchall()[0]

    def getUserGroups(self, userName):
        mycursor = self.mydb.cursor()
        mycursor.execute('Call getUserGroups(\'{}\')'.format(userName))
        return mycursor.fetchall()[0][0]

    def addPendingGroupMembership(self, userid, groupid):
        mycursor = self.mydb.cursor()
        mycursor.execute('Call addPendingGroupMembership(\'{}\', \'{}\')'.format(userid, groupid))
        return mycursor.fetchall()[0][0]

    def addUserGroupMembership(self, userName, groupid):
        mycursor = self.mydb.cursor()
        mycursor.execute('Call addUserGroupMembership(\'{}\', \'{}\')'.format(userName, groupid))
        return mycursor.fetchall()[0][0]

    def deletePendingGroupMembership(self, userid, groupid):
        mycursor = self.mydb.cursor()
        mycursor.execute('Call deletePendingGroupMembership(\'{}\', \'{}\')'.format(userid, groupid))
        return mycursor.fetchall()[0][0]

    def login(self, username, password):
        mycursor = self.mydb.cursor()
        mycursor.execute('select count(*) from users where username = \'{}\' and user_password = \'{}\''
                        .format(username, password))
        value = mycursor.fetchall()[0][0]
        if value:
            return hashlib.sha256((username + password).encode()).hexdigest() # todo what is token
        else:
            return False

    def createGroup(self, population, groupname, icon, public, inviteonly, description, user_id):
        mycursor = self.mydb.cursor()
        mycursor.execute('INSERT INTO watchdog_groups '
                         '(population, capacity,group_name,image,public_visibility,invite_only,group_description, owner_id, siren_active) '
                         'VALUES (0, \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', 0)'
                         .format(population, groupname, icon, public, inviteonly, description, user_id))

        self.mydb.commit()

    def deleteGroup(self, groupid, userid):

        if self.getGroupOwner(groupid) != userid:
            return 'You are not the owner'
        mycursor = self.mydb.cursor()
        mycursor.execute('delete from watchdog_groups where group_id = \'{}\''.format(groupid))
        self.mydb.commit()
        return 'Deleted'

    def getGroupInfo(self, groupid):
        """
        todo cannot figure out how to get an image
        :param groupId:
        :return:
        """
        mycursor = self.mydb.cursor()
        mycursor.execute('SELECT * FROM watchdog_groups where group_id = {}'.format(groupid))
        data = mycursor.fetchall()[0]
        #image =  Image.open(data[4])
        #return data[:4] + [image] + data[5:]
        return data[:4] + data[5:]


    def getGroupMembers(self, groupid):
        mycursor = self.mydb.cursor()
        mycursor.execute('select a.first_name, a.last_name from user_group_memberships join users a using(user_id) where group_id = {}'.format(groupid))
        return mycursor.fetchall()


    def getGroupOwner(self, groupid):
        mycursor = self.mydb.cursor()
        mycursor.execute('select owner_id from watchdog_groups where group_id = {}'.format(groupid))
        return mycursor.fetchall()[0][0]


    def createUser(self, username, password, firstName, lastName, email, city, country):
        mycursor = self.mydb.cursor()
        mycursor.execute('insert into users (username,email,user_password,first_name,last_name,location)'
                         'values (\'{}\',\'{}\',\'{}\',\'{}\',\'{}\',\'{}\')'
                         .format(username, email, password, firstName, lastName, city + ', ' + country))
        self.mydb.commit()


    def deleteUser(self, username, password):
        mycursor = self.mydb.cursor()
        mycursor.execute('delete from users where username = \'{}\' and user_password = \'{}\''.format(username, password))
        self.mydb.commit()


    def validateUser(self, user_id, groupid):
        mycursor = self.mydb.cursor()
        mycursor.execute('select * from user_group_memberships '
                         ' where group_id = \'{}\' and user_id = \'{}\';'
                         .format(groupid, user_id))
        ingroup = mycursor.fetchall()
        if len(ingroup) == 0:
            return False
        return True


    def changeCapacity(self, groupid, userid, capacity):
        if self.getGroupOwner(groupid) != userid:
            return 'You Are Not The Group Owner'
        mycursor = self.mydb.cursor()
        mycursor.execute('update watchdog_groups set capacity = \'{}\' where group_id = \'{}\''
                         .format(capacity, groupid))
        self.mydb.commit()


    def changeGroupName(self, groupid, userid, groupname):
        if self.getGroupOwner(groupid) != userid:
            return 'You Are Not The Group Owner'
        mycursor = self.mydb.cursor()
        mycursor.execute('update watchdog_groups set group_name = \'{}\' where group_id = \'{}\''
                         .format(groupname, groupid))
        self.mydb.commit()


    def changePrivacy(self, groupid, userid, privacy):

        if self.getGroupOwner(groupid) != userid:
            return 'You Are Not The Group Owner'
        mycursor = self.mydb.cursor()
        mycursor.execute('update watchdog_groups set public_visibility = \'{}\' where group_id = \'{}\''
                         .format(privacy, groupid))
        self.mydb.commit()


    def changeInviteOnly(self, groupid, userid, invite):
        if self.getGroupOwner(groupid) != userid:
            return 'You Are Not The Group Owner'
        mycursor = self.mydb.cursor()
        mycursor.execute('update watchdog_groups set invite_only = \'{}\' where group_id = \'{}\''
                         .format(invite, groupid))
        self.mydb.commit()


    def changeGroupDescription(self, groupid, userid, desciption):
        if self.getGroupOwner(groupid) != userid:
            return 'You Are Not The Group Owner'
        mycursor = self.mydb.cursor()
        mycursor.execute('update watchdog_groups set group_description = \'{}\' where group_id = \'{}\''
                         .format(desciption, groupid))
        self.mydb.commit()

    def changeGroupOwner(self, groupid, userid, desciption):
        if self.getGroupOwner(groupid) != userid:
            return 'You Are Not The Group Owner'

        if self.validateUser(userid, groupid):
            mycursor = self.mydb.cursor()
            mycursor.execute('update watchdog_groups set owner_id = \'{}\' where group_id = \'{}\''
                         .format(desciption, groupid))
            self.mydb.commit()
        else:
            return ('User is not in group')




