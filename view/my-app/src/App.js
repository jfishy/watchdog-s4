import './App.css';
import Home from './components/Home/Home';
import Login from './components/User_Registration/Login';
import Register from './components/User_Registration/Register';
import {  BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {

  return (
    <div className="wrapper">
      <Router>
        {
          <Switch>
            <Route
              path="/register"
              render={props => <Register {...props}/>}
            />
            <Route
              path="/home"
              render={props => <Home {...props}/>}
            />
            <Route
              path="/"
              render={props => <Login {...props}/>}
            />
          </Switch>
        }
      </Router>
  </div>
  );
}

export default App;
