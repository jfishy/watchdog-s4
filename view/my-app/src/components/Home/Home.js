import React from 'react';
import Sidebar from './Sidebar'
import Header from './Header'
import "./Home.css"

import { AiOutlineUsergroupAdd } from "react-icons/ai";

export default function Home() {
  return(
    <div className="home-wrapper">
      <Sidebar />
      <div className="main-container">
        <Header />
        <button id="button-container">
          <div id = "add-btn">
          <AiOutlineUsergroupAdd size="2x"/>
          <p id= "new-group"> New Group</p>
          </div> 
        </button>
      </div>
    </div>
  
  );
}