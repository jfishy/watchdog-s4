import { Toolbar } from "@material-ui/core";
import React from "react";
import './Header.css';
import moment from "moment";

export default function Header() {
  const date = moment().format("DD-MM-YYYY hh:mm:ss");
  return (
    <header>
      <Toolbar className="topbar">
         <p id="date"> { date } </p> 
      </Toolbar>
    </header>
  );
}