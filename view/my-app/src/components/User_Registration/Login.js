import React, {useState} from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../resources/logo.png'
import "./Login.css"


  async function loginUser(credentials) {
    return fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
      .then(data => {
        console.log(data)
        if(data.status === 200){
          return data.json()
        } else {
          return null
        }
      })
   }

export default function Login() {
  // for login
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const history = useHistory();

    const isValidString =(string) => {
      return string && string.length > 0;
    }

    const handleLogin = async e => {
        e.preventDefault();
        if(isValidString(username) && isValidString(password)){
          const token = await loginUser({
            username,
            password
          });
          console.log("token" + token)
          if(token){
            history.push('/home')
          }
        }
      }
    
      const handleNewUser = () => {
        history.push('/register')
      }

    
    return(
        <div className="login-wrapper">
        <div className="logo-container"><img src={logo} className="logo" /></div>
        <h1 className="header">Watchdog</h1>
        <form id = "form" onSubmit={handleLogin}>
            <label>
            <p>Username</p>
            <input type="text" onChange={e => setUserName(e.target.value.trim())}/>
            </label>
            <label>
            <p>Password</p>
            <input type="password" onChange={e => setPassword(e.target.value.trim())}/>
            </label>
            <div>
            <button id = "login-button" type="submit"> { "Login" }</button>
            </div>
        </form>
        { <button id = "new-user" onClick={ handleNewUser }>Click to Create an Account </button> }
        </div>
    )
  }