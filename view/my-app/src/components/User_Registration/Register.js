import React, {useState} from 'react';
import { useHistory } from 'react-router-dom';
import './Register.css'



async function registerUser(credentials) {
    return fetch('/createuser', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
      .then(data => {
          if(data.status === 200){
              return "Success"
          } else {
              return null;
          }
      })
    }

export default function Register() {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [city, setCity] = useState();
    const [country, setCountry] = useState();
    const history = useHistory();

    const isValidString =(string) => {
        return string && string.length > 0;
      }

    const handleRegistration = async e => {
        e.preventDefault();
        if(isValidString(username) && isValidString(password) && isValidString(firstName) && isValidString(lastName)
        && isValidString(email) && isValidString(city) && isValidString(country)){
            const response = await registerUser({
            username,
            password,
            firstName,
            lastName,
            email,
            city,
            country
            });
            console.log(response)
            if(response === "Success"){
                history.push('/home')
            }
        }
      }
    return (
        <div id = "container">
            <h1 className="header">Create New Account</h1>
            <form id = "register-form" onSubmit={handleRegistration}>
                <label>
                <p>Username</p>
                <input type="text" onChange={e => setUserName(e.target.value.trim())}/>
                </label>
                <label>
                <p>Password</p>
                <input type="password" onChange={e => setPassword(e.target.value.trim())}/>
                </label>
                <label>
                <p>First Name</p>
                <input type="firstName" onChange={e => setFirstName(e.target.value.trim())}/>
                </label>
                <label>
                <p>Last Name</p>
                <input type="lastName" onChange={e => setLastName(e.target.value.trim())}/>
                </label>
                <label>
                <p>Email</p>
                <input type="email" onChange={e => setEmail(e.target.value.trim())}/>
                </label>
                <label>
                <p>City</p>
                <input type="city" onChange={e => setCity(e.target.value.trim())}/>
                </label>
                <label>
                <p>Country</p>
                <input type="country" onChange={e => setCountry(e.target.value.trim())}/>
                </label>
                <div>
                    <button id = "login-button" type="submit"> {  "Create Account" }</button>
                </div>
            </form>
        </div>
    )
}