# README #



### What is this repository for? ###

* Quick summary		
	This is team's 4 final project for EECE4520, project Watchdog.
* Version 
	1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	This application is setup using a flask framework. Look under the dependencies section to make sure you have all the required packages to run the application. You will also need npm and node installed. This application uses a mySQL database to show the generated data on the application, be sure to follow the setup instruction located in that associated README file as well.
	
* Configuration
* Dependencies
	Pip install the following modules to get the project working:
			flask-restful
			mysql
			mysql-connector
			mysql-connector-python
			
	Once npm is installed, run 'npm install' to download its associated packages, needed for this application.
		
* Database configuration
	Read the README in the /sql_server/ folder uner the main directory to get the database setup on your local machine. 
* How to run tests
* Deployment instructions
	To run this you will need to open two terminals to do the following:
	
		In the first terminal: we will run the flask framework. From project directory:
			cd controller
			source venv/bin/activate
			flask run

		The second terminal will be used to run the runtime enironment. From project directory:
			cd view 
			npm start

		This should automatically open up a browser with the login page of the application. Try the following login to see an example of implemented features:
			username: angelica.holder
			password: afab74

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact