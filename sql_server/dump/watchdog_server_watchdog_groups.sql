-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: watchdog_server
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `watchdog_groups`
--

DROP TABLE IF EXISTS `watchdog_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `watchdog_groups` (
  `group_id` int NOT NULL AUTO_INCREMENT,
  `population` int DEFAULT NULL,
  `capacity` int DEFAULT NULL,
  `group_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `image` blob,
  `public_visibility` tinyint DEFAULT NULL,
  `invite_only` tinyint DEFAULT NULL,
  `group_description` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `owner_id` int DEFAULT NULL,
  `siren_active` tinyint DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `watchdog_groups`
--

LOCK TABLES `watchdog_groups` WRITE;
/*!40000 ALTER TABLE `watchdog_groups` DISABLE KEYS */;
INSERT INTO `watchdog_groups` VALUES (1,4,15,'Family',_binary 'watchdog-s4\\resources\\family_1.png',0,1,'For our family of ten\'s use only. Don\'t invite anybody else!!',3,0),(2,3,15,'Family',_binary 'watchdog-s4\\resources\\family_2.png',0,1,'Don\'t forget to take out the trash on Mondays and Wednesdays',1,1),(3,12,30,'EECE4520 teammates',_binary 'watchdog-s4\\resources\\eece4520_teammates.jpg',0,1,'For SP2021 EECE4520 Team 4',6,0),(4,6,15,'Stop&Shop Employees',_binary 'watchdog-s4\\resources\\stop_and_shop.png',1,0,'Cleanup on aisle-6',14,1),(5,9,15,'Hydro homies',_binary 'watchdog-s4\\resources\\hydro_homies.jpg',1,0,'5 liters of water per day at minimum',1,0);
/*!40000 ALTER TABLE `watchdog_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-16 14:05:00
