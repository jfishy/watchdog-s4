-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: watchdog_server
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `user_password` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `first_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `location` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'angelica.holder','angelica.holder@gmail.com','afab74','Angelica','Holder','Podgorica, Montenegro'),(2,'ayva.knight','ayva.knight@gmail.com','74af8b','Ayva','Knight','Panama City, Panama'),(3,'robbie.bowen','robbie.bowen@gmail.com','7774af','Robbie','Bowen',''),(4,'caris.pitts','caris.pitts@gmail.com','18146c','Caris','Pitts','Male, Maldives'),(5,'kiara.nava','kiara.nava@gmail.com','acabc4','Kiara','Nava','Guangzhou, China'),(6,'chantal.phelps','chantal.phelps@gmail.com','c4abb6','Chantal','Phelps','Doha, Qatar'),(7,'woodrow.mcgee','woodrow.mcgee@gmail.com','ed82b0','Woodrow','McGee','Ljubljana, Slovenia'),(8,'miriam.pate','miriam.pate@gmail.com','a8ed82','Miriam','Pate','Milan, Italy'),(9,'yusha.rojas','yusha.rojas@gmail.com','ed8282','Yusha','Rojas','Dhaka, Bangladesh'),(10,'felicia.norman','felicia.norman@gmail.com','f9d7d7','Felicia','Norman','Tel Aviv, Israel'),(11,'arandeep.harrell','arandeep.harrell@gmail.com','221593','Arandeep','Harrell',''),(12,'tom.kinney','tom.kinney@gmail.com','19448f','Tom','Kinney',''),(13,'fardeen.benson','fardeen.benson@gmail.com','198f2d','Fardeen','Benson','Lisbon, Portugal'),(14,'belle.marin','belle.marin@gmail.com','a0eead','Belle','Marin','Bordeaux, France'),(15,'lea.nunez','lea.nunez@gmail.com','002ed6','Lea','Nunez','Milan, Italy'),(16,'pawel.shepherd','pawel.shepherd@gmail.com','8670ff','Pawel','Shepherd','Cairo, Egypt'),(17,'ollie.matthews','ollie.matthews@gmail.com','18009e','Ollie','Matthews','Beirut, Lebanon'),(18,'charley.norris','charley.norris@gmail.com','54009e','Charley','Norris','Dallas, USA'),(19,'gracie-leigh.charles','gracie-leigh.charles@gmail.com','009e5a','Gracie-Leigh','Charles','Kyiv, Ukraine'),(20,'teddy.haworth','teddy.haworth@gmail.com','9e2a00','Teddy','Haworth','Liverpool, UK'),(21,'owain.howarth','owain.howarth@gmail.com','009e5c','Owain','Howarth','Sarajevo, Bosnia & Herzegovina'),(22,'warwick.legge','warwick.legge@gmail.com','70ffc4','Warwick','Legge','Helsinki, Finland'),(23,'hanan.redfern','hanan.redfern@gmail.com','c105eb','Hanan','Redfern','Las Vegas, USA'),(24,'falak.sweeney','falak.sweeney@gmail.com','f94db2','Falak','Sweeney','Wellington, New Zealand'),(25,'savannah.maynard','savannah.maynard@gmail.com','569405','Savannah','Maynard','Toronto, Canada');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-16 14:05:00
