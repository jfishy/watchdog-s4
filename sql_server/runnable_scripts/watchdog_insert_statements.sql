# Please read the README.txt in the mysql folder
# INSERT statements generated using http://tools.perceptus.ca/text-wiz.php

# Clear all tables first

TRUNCATE watchdog_server.users;
TRUNCATE watchdog_server.watchdog_groups;
TRUNCATE watchdog_server.user_group_memberships;
TRUNCATE watchdog_server.group_membership_pending;

# user inserts
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('angelica.holder','angelica.holder@gmail.com','afab74','Angelica','Holder','Podgorica, Montenegro');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('ayva.knight','ayva.knight@gmail.com','74af8b','Ayva','Knight','Panama City, Panama');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('robbie.bowen','robbie.bowen@gmail.com','7774af','Robbie','Bowen','');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('caris.pitts','caris.pitts@gmail.com','18146c','Caris','Pitts','Male, Maldives');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('kiara.nava','kiara.nava@gmail.com','acabc4','Kiara','Nava','Guangzhou, China');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('chantal.phelps','chantal.phelps@gmail.com','c4abb6','Chantal','Phelps','Doha, Qatar');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('woodrow.mcgee','woodrow.mcgee@gmail.com','ed82b0','Woodrow','McGee','Ljubljana, Slovenia');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('miriam.pate','miriam.pate@gmail.com','a8ed82','Miriam','Pate','Milan, Italy');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('yusha.rojas','yusha.rojas@gmail.com','ed8282','Yusha','Rojas','Dhaka, Bangladesh');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('felicia.norman','felicia.norman@gmail.com','f9d7d7','Felicia','Norman','Tel Aviv, Israel');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('arandeep.harrell','arandeep.harrell@gmail.com','221593','Arandeep','Harrell','');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('tom.kinney','tom.kinney@gmail.com','19448f','Tom','Kinney','');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('fardeen.benson','fardeen.benson@gmail.com','198f2d','Fardeen','Benson','Lisbon, Portugal');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('belle.marin','belle.marin@gmail.com','a0eead','Belle','Marin','Bordeaux, France');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('lea.nunez','lea.nunez@gmail.com','002ed6','Lea','Nunez','Milan, Italy');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('pawel.shepherd','pawel.shepherd@gmail.com','8670ff','Pawel','Shepherd','Cairo, Egypt');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('ollie.matthews','ollie.matthews@gmail.com','18009e','Ollie','Matthews','Beirut, Lebanon');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('charley.norris','charley.norris@gmail.com','54009e','Charley','Norris','Dallas, USA');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('gracie-leigh.charles','gracie-leigh.charles@gmail.com','009e5a','Gracie-Leigh','Charles','Kyiv, Ukraine');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('teddy.haworth','teddy.haworth@gmail.com','9e2a00','Teddy','Haworth','Liverpool, UK');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('owain.howarth','owain.howarth@gmail.com','009e5c','Owain','Howarth','Sarajevo, Bosnia & Herzegovina');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('warwick.legge','warwick.legge@gmail.com','70ffc4','Warwick','Legge','Helsinki, Finland');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('hanan.redfern','hanan.redfern@gmail.com','c105eb','Hanan','Redfern','Las Vegas, USA');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('falak.sweeney','falak.sweeney@gmail.com','f94db2','Falak','Sweeney','Wellington, New Zealand');
INSERT INTO users (username,email,user_password,first_name,last_name,location) VALUES ('savannah.maynard','savannah.maynard@gmail.com','569405','Savannah','Maynard','Toronto, Canada');

# group inserts
INSERT INTO watchdog_groups (population,capacity,group_name,image,public_visibility,invite_only,group_description,owner_id,siren_active) VALUES ('4','15','Family','watchdog-s4\\resources\\family_1.png','0','1','For our family of ten\'s use only. Don\'t invite anybody else!!','3','0');
INSERT INTO watchdog_groups (population,capacity,group_name,image,public_visibility,invite_only,group_description,owner_id,siren_active) VALUES ('3','15','Family','watchdog-s4\\resources\\family_2.png','0','1','Don\'t forget to take out the trash on Mondays and Wednesdays','1','1');
INSERT INTO watchdog_groups (population,capacity,group_name,image,public_visibility,invite_only,group_description,owner_id,siren_active) VALUES ('12','30','EECE4520 teammates','watchdog-s4\\resources\\eece4520_teammates.jpg','0','1','For SP2021 EECE4520 Team 4','6','0');
INSERT INTO watchdog_groups (population,capacity,group_name,image,public_visibility,invite_only,group_description,owner_id,siren_active) VALUES ('6','15','Stop&Shop Employees','watchdog-s4\\resources\\stop_and_shop.png','1','0','Cleanup on aisle-6','14','1');
INSERT INTO watchdog_groups (population,capacity,group_name,image,public_visibility,invite_only,group_description,owner_id,siren_active) VALUES ('9','15','Hydro homies','watchdog-s4\\resources\\hydro_homies.jpg','1','0','5 liters of water per day at minimum','1','0');

#user_group_memberships inserts
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('1','1');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('1','2');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('1','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('1','4');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('1','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('2','1');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('2','2');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('3','1');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('3','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('4','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('5','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('6','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('7','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('8','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('9','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('10','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('11','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('12','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('13','3');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('14','4');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('15','4');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('16','4');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('17','4');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('18','1');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('18','4');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('18','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('18','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('19','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('20','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('21','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('22','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('23','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('24','5');
INSERT INTO user_group_memberships (user_id,group_id) VALUES ('25','2');

#group_membership_pending inserts
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('10','1');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('12','1');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('17','1');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('3','2');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('20','2');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('21','2');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('14','3');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('25','3');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('18','4');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('3','5');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('6','5');
INSERT INTO group_membership_pending (user_id,group_id) VALUES ('7','5');
