use watchdog_server;

######################################################################################
################################### GET PROCEDURES ###################################
######################################################################################
## Get userId with username
drop procedure if exists getUserId;
delimiter // 
create procedure getUserId
(
	in userName varchar(255)
)
begin
	declare user_id_var int;
    
	select users.user_id
	from users
	where users.username = userName;
    
end //
delimiter ;
######################################################################################
## Get all user info
drop procedure if exists getUserInfo;
delimiter //
create procedure getUserInfo
(
	in userName varchar(255)
)
begin
	declare user_id_var int;
    
    # Get userId with username
    # Can't figure out how to call the exusting getUserId SP to remove redundancy..
	set user_id_var = (
		select users.user_id
		from users
		where users.username = userName);
    
    select *
    from users
    where users.user_id = user_id_var;
    
end //
delimiter ;
######################################################################################
## Get user groups procedure
drop procedure if exists getUserGroups;
delimiter //
create procedure getUserGroups
(
	in userName varchar(255)
)
begin
	declare user_id_var int;

	# Find the user's user_id with the input userName
	set user_id_var = (
		select users.user_id
		from users
		where users.username = userName);
    
    # Get all group_id's the user is a part of
    select user_group_memberships.group_id
    from user_group_memberships
    where user_group_memberships.user_id = user_id_var;

end //
delimiter ;

######################################################################################
################################ INSERT PROCEDURES ###################################
######################################################################################
## Add a pending invite to group_membership_pending
drop procedure if exists addPendingGroupMembership;
delimiter //
create procedure addPendingGroupMembership
(
	in userId int,
    in groupId int
)
begin
	insert into group_membership_pending(user_id, group_id) values (userId, groupId);
end //
delimiter ;
######################################################################################
## Add a group to user_group_memberships
drop procedure if exists addUserGroupMembership;
delimiter //
create procedure addUserGroupMembership
(
	in userName varchar(255),
    in groupId int
)
begin
	declare user_id_var int;
    declare group_id_var int;
    
    
    
end //
delimiter ;

######################################################################################
################################ DELETE PROCEDURES ###################################
######################################################################################
drop procedure if exists deletePendingGroupMembership;
delimiter //
create procedure deletePendingGroupMembership
(
	in userId int,
    in groupId int
)
begin
	delete from group_membership_pending where user_id = userId and group_id = groupId;
end //
delimiter ;

