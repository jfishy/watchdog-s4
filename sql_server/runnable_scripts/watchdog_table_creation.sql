# Use this if you ever need to reset all your tables
# Follow with using 'watchdog_insert_statements.sql' script to restore all entries
# All entries stored in 'data_entries.xlsx" file

# ALL CHANGES TO TABLES SHOULD ALSO BE REFLECTED IN THIS SCRIPT

drop database if exists watchdog_server;
create database watchdog_server;
use watchdog_server;

## Users
drop table if exists users;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `username` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL UNIQUE,
  `email` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL UNIQUE,
  `user_password` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `first_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `location` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
);

## Groups
drop table if exists watchdog_groups;
CREATE TABLE `watchdog_groups` (
  `group_id` int NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `population` int DEFAULT NULL,
  `capacity` int DEFAULT NULL,
  `group_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `image` blob,
  `public_visibility` tinyint DEFAULT NULL,
  `invite_only` tinyint DEFAULT NULL,
  `group_description` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `owner_id` int not null,
  `siren_active` bool not null
);

## group_membership_pending
drop table if exists group_membership_pending;
CREATE TABLE `group_membership_pending` (
  `group_membership_pending_id` int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  `user_id` int NOT NULL,
  `group_id` int NOT NULL
);

## user_group_memberships
drop table if exists user_group_memberships;
CREATE TABLE `user_group_memberships` (
  `user_group_membership_id` int NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `user_id` int DEFAULT NULL,
  `group_id` int DEFAULT NULL
);