# Use for testing stored procedures

use watchdog_server;

call getUserId("angelica.holder");
call getUserInfo("angelica.holder");
call getUserGroups("angelica.holder");

call addPendingGroupMembership(7, 4);
call deletePendingGroupMembership(7, 4);