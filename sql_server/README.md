This README file covers the installation steps and setup for the mySQL server to allow interaction with the watchdog server, as well as a quick summary of the contents in each table and included procedures. If you have any questions pertaining to these things, feel free to ask Wilkin or Justin.

Note: We decided it was easiest if everyone installed and setup the server locally and not try to deal with setting up an online server since after some research, this would cause many more complications like getting licenses, testing, etc. If anyone thinks of a simpler way to do this, don't hesitate to bring it up.

Setup Steps:
	Please refer to your appropriate operating system for the next steps.

	Windows:
	Here's a video for Window set-up: https://youtu.be/2HQC94la6go
		Download:
			1. Go to https://dev.mysql.com/downloads/mysql/
			2. In the 'MySQL Community Downloads' screen, under 'Select Operating System', select 'Microsoft Windows'.
			3. Under 'Recommended Download', click on the 'MySQL Installer for Windows'.
			4. Next to the 'Windows (x86, 32-bit)', 'MSI Installer', click the 'Download' button.
			5. In the 'Login Now' or 'Sign Up' screen, click on 'No thanks, just start my download'.
			6. If the browser asks What you want to do when installer is done downloading, select 'Run'. Otherwise, run the MSI installer once it's done downloading
			7. If the 'User Account Control' window asks 'Do you want to allow this app to make changes to your device?' answer 'Yes'.
		
		Installation:
			1. In the 'Choosing a Setup Type' screen, select 'Custom' and click 'Next'.
			2. In the 'Select Products and Features' screen, under 'Available Products' expand 'MySQL Servers', choose the latest server, e.g., 'MySQL Server 8.0.22', and click the right arrow to add it to 'Products/Features To Be Installed'.
			3. In the 'Select Products and Features' screen, under 'Available Products' expand 'Applications' and then under 'MySQL Workbench', select the latest client, e.g., 'MySQL Workbench 8.0', click the right arrow to add it to 'Products/Features To Be Installed'.
			4. After adding 'MySQL Server' and 'MySQL Workbench' to 'Products/Features To Be Installed', click 'Next'.
			5. In the 'Installation' screen, click 'Execute'. Wait while the products are downloaded and installed.
			6. Once all product installation 'Status' is 'Complete', click 'Next'.
			7. In the 'Product Configuration' screen, click 'Next'.
			
		Configuration for MySQL Community Server:
			1. In the 'High Availability' screen, select 'Standalone MySQL Server', and click 'Next'.
			2. In the 'Type' and 'Networking' screen, leave the defaults and click 'Next'.
			3. In the 'Authentication Method' screen, select 'Use Strong Password Encryption for Authentication' and click 'Next'.
			4. In the 'Accounts and Roles' screen, enter a password for the root account. If you forget the root password, you will need to reinstall the database server, so don't forget it! Click 'Next'.
			5. In the 'Windows Service' screen, select the following checkboxes and then click 'Next'.
				a. Configure MySQL Server as a Windows Server
				b. Start the MySQL Server at System Startup
			6. In the 'Apply Configuration' screen click 'Execute' and then 'Finish'.
	MacOS:
	Note: You will have to complete the download and installation for the Server and Workbench separately.
		Download:
			1. Go to https://dev.mysql.com/downloads/mysql/
			2. In the MySQL Community Downloads screen, under Select Operating System, select macOS
			3. Next to the macOS 10.15 (x86, 64-bit), DMG Archive, click the Download button
			4. In the Login Now or Sign Up screen, click on No thanks, just start my download
			5. Wait for the DMG installer to download into your Downloads folder

		From your Downloads folder, double click the downloaded DMG archive to run it and follow these steps
			1. In the DMG window, double click the PKG file to run the installer
			2. If the installer tries to determine if the software can be installed, click on Continue
			3. In the Introduction screen, click Continue
			4. In the License screen, click Continue and then Agree5
			5. In the Installation screen, click Install
			6. If macOS asks for your password, type it in to allow the installation to start, and click Install Software
			7. In the Configuration screen, select Use Strong Password Encryption and click Next
			8. Enter a password for the root user. If you forget the root password, you will need to reinstall the database server, so don't forget it!. Click Start MySQL Server once the installation is complete. Click Finish
			9. If macOS asks for your password again, type it in to complete the installation to start, and click OK
			10. In the Summary screen, click Close

		Starting and stopping the server from macOS System Preferences
			1. To start and stop the server, go to macOS System Preferences, e.g., select Apple Menu at the top left, then System Preferences
			2. In the System Preferences screen, scroll to the bottom and find the new MySQL icon
			3. Double click the MySQL icon to open its preferences screen
			4. In the MySQL preferences screen, in the Instances tab, notice the server can be stopped with the Stop MySQL Server button. Make sure the checkbox Start MySQL when your computer starts up, is checked

		Repeat the same installation steps for the MySQL workbench found at the following link: https://dev.mysql.com/downloads/workbench/
		
		Select Operating System as macOS and click the Download button. Skip the Login and Sign Up if you want by clicking the No thanks, just start my download link. Once the download completes, open the downloaded DMG file, execute the package file, and follow the installation instructions
		
	Setting up/Resetting the watchdog_server schema:
	Note: These are two methods to do the same thing. The first one should be easier, but if you don't want to reset all of the data on your server, refer to the second method.
		
		Importing the database:
			1. Select the 'watchdog_server' schema by double clicking it in the SCHEMAS window under your Navigator on the left side; it will be bolded when selected. If you don't have the schema, create one by clicking the appropriate icon from the top bar. Ensure the name is the same when creating.
			2. Click the 'Server' tab at the top of the screen to open the drop down menu and click 'Data Import.'
			3. From Import options, select the Import from the Dump Project Folder opttion and put the PATH to the sql-server\dump folder within the BitBucket repository.
			4. You should see a 'watchdog_server' schema appear below under the 'Select Database Object to Import' window. The schema should be selected and all the tables within it.
			5. Ensure that 'Dump structure and Data' is selected from the drop down menu and click 'Start Import'. All the tables, stored procedures, and data will appear under the schema.
		
		Running Individual Scripts
				1. Ensure your mysql server is running. Open a sql script and navigate to the sql_server\runnable_scripts folder in your repo. Choose the appropriate script as described below.
				2. Run 'watchdog_table_creation.sql' script to create all tables with appropriate characteristics
				3. Run 'watchdog_insert_statements.sql' script to insert data into all tables
					a. In the worst case scenario, all dummy data has been stored in the 'data_entries.xlsx' file and an online INSERT statement creation tool can be used
				4. Run 'watchdog_storedProcedures.sql' script to setup all stored procedures
				5. The watchdog_server schema is now ready for use
	
	Exporting (probably shouldn't ever need to do):
		Export:
			1. Select the 'watchdog_server' schema by double clicking it in the SCHEMAS window under your Navigator on the left side. It will be bolded when selected.
			2. Click the 'Server' tab at the top of the screen to open the drop down menu and click 'Data Export.'
			3. From the 'Table to Export' selecet the watch_dog server schema. On the right side select all the schema objects.
			4. Under 'Objects to Export' enable the 'Dump Stored Procedures and Functions' option. 
			5. Under 'Export Option', choose to export to a project folder. Place the path to the sql-server\dump folder in the repo. Make sure the previous direcory is deleted or else it will duplicate it. It says it will overwrite it but it doesn't actually.
	

	
			
Introduction Resource:
As an extra resource, included is the first homework assignment from a SP21 Database Design course, which covers the installation included in this file, as well as a introductory tutorial for interacting with mySQL: https://docs.google.com/document/d/17PRkqOX03Ydv9QSmmqU-ifmuxqCuoWn6sAjxUYB1ceE/edit

Server Summary:
Note: Avoid pushing any changes of the tables and procedures onto BitBucket if possible. If a change needs to be made, let others know, in case any scripts need to be changed accordingly. Otherwise feel free to manipulate any data and do as much testing as you'd like on your local machine.

Tables:
When viewing the watchdog_server schema, you will find two tables. Each table is broken down below:

	User: 
	This table represents a Watchdog User as shown on the class diagram.
		Fields:
		user_id 		- A unique id to represent each user account.
		username 		- A unique string relating to a user. Max length of 45 characters.
		email			- A unique string relating to a user. Max length of 45 characters. 
		password		- The string that the user will enter to login. Max length of 45 characters.
		first_name		- The string showing the user's first name. Max length of 45 characters.
		last_name 		- The string showing the user's last name. Max length of 45 characters.
		location		- The string representing a user's location. Max length of 45 characters.
		
	Group:
	This table represents a Group with fields corresponding to the class diagram.
		Fields:
		id				- A unique id to represent each group as some may have the same name.
		population		- An int showing current number of users in the group.
		capacity		- An int showing the maximum number of users that should be allowed in the group.
		name			- A string representing the name of the group. Max length of 45 characters.
		image			- A blob item that holds an image. Images usually aren't stored on databases due to their size but for our purposes, it should suffice.
		public_visibility 	- A boolean that shows whether a group is viewable to everyone.
		invite_only		- A boolean that determines if users not in the group can be invited.
		members			- An ID to the mapping table for the list of members.
		requests		- An ID to the mapping table for the list of pending requests, both in and out.
		description		- A string to describe the group, list rules, etc. Max length of 256 characters.
		
Stored Procedures (SP):
Procedures that can be used in code to interact with the database, think of them as functions (mySQL has something called functions but they are actually used in the stored procedures).
If you would like to implement a new procedure feel free to reach out to Wilkin if you'd like help.

	Using SP's in Python
		NOTE: '$' denote beginnings of lines of code
		NOTE: The mysql server must be running before you try to establish a connection with the Python script. You may verify this by going to Server/Server Status in MySQL Workbench with the server open
		1. Include mysql connector library with the following two lines of code
			a. $ import mysql.connector
			b. $ from mysql.connector import (connection)
		2. Initialize connection with mysql. FIll in arguments with appropriate inputs based on your server connection
			a. $ cnct = mysql.connector.connect(user = 'root', password = '', host = 'localhost', port = '3306', database = 'watchdog_server')
		3. Create an empty var or list to store your query results
			a. $ myList = []
		3. Call SQL stored procedures
			a. Create a cursor
				i. $ cursor = cnct.cursor()
			b. Call the stored procedure by name with Python arguments
				i. $ cursor.callproc('spNameHere', [arg1, arg2, arg3, argX])
			c. Get returned query results
				i. $ result = cursor.stored_results()
		4. Store query results in a variable. Now you can use the query results in Python.
			a. $ for i in result: myList.append(i.fetchall())
			b. NOTE: You may need to cast types. By default, query results are strings
		5. Each time cursor.callproc(spNameHere) is called, the previous stored_results are lost and replaced
		6. Done

Coming Soon...